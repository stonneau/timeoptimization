cmake_minimum_required(VERSION 2.8.3)
project(solver)

#########
# flags #
#########

set(BUILD_DOCUMENTATION OFF)
list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake )
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x -g -Wall -Wwrite-strings -pedantic -O3 -funroll-loops -fPIC")

#####################
# required packages #
#####################

set(CATKIN_PKGS ${CATKIN_PKGS} yaml_cpp)

find_package (Eigen3 REQUIRED)
find_package(catkin REQUIRED COMPONENTS ${CATKIN_PKGS})
find_package(Boost REQUIRED COMPONENTS system program_options filesystem)

#######################
# include directories #
#######################

catkin_package(
  LIBRARIES solver
  CATKIN_DEPENDS yaml_cpp
  INCLUDE_DIRS include ${EIGEN3_INCLUDE_DIR}
)

get_filename_component(TEST_PATH tests ABSOLUTE)

include_directories(
  tests
  include
  ${EIGEN3_INCLUDE_DIR}
  ${GTEST_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  ${catkin_INCLUDE_DIRS}
)

################
# source files #
################

set(IPsolver_SRC_FILES ${IPsolver_SRC_FILES}
  src/solver/interface/Var.cpp
  src/solver/interface/Cone.cpp
  src/solver/interface/Exprs.cpp
  src/solver/interface/Model.cpp
  src/solver/interface/OptVar.cpp
  src/solver/interface/SolverSetting.cpp
  
  src/solver/optimizer/IPSolver.cpp
  src/solver/optimizer/LinSolver.cpp
  src/solver/optimizer/EqRoutine.cpp
  src/solver/optimizer/InfoPrinter.cpp
  src/solver/optimizer/SparseCholesky.cpp
)

#####################
# linking libraries #
#####################

set(solver_LIBS
  m
  ${Boost_LIBRARIES}
  ${catkin_LIBRARIES}
)
IF(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
  set(solver_LIBS ${solver_LIBS} rt)
ENDIF(${CMAKE_SYSTEM_NAME} MATCHES "Linux")

###################
# build libraries #
###################

add_library(solver ${IPsolver_SRC_FILES})
target_link_libraries(solver ${solver_LIBS})

#########
# tests #
#########

catkin_add_gtest(solver_tests
  tests/gtest_main.cpp
  tests/test_solver.cpp
)
target_link_libraries(solver_tests solver)
set_target_properties(solver_tests PROPERTIES COMPILE_DEFINITIONS TEST_PATH="${TEST_PATH}/yaml_config_files/")